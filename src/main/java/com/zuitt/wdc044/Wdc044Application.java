package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Wdc044Application {

	public static void main(String[] args) {

		SpringApplication.run(Wdc044Application.class, args);
	}

	//@RequestParam - is used to extract query parameters, form parameters, and even files from the request.

		// name = John; Hello John.  example
		// to append the URL with the name
		//http://localhosts:8080/

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name",defaultValue = "World")String name) {
		return String.format("Hello %s!", name);
	}

	@GetMapping("/greetings")
	public String greetings(@RequestParam(value = "greet",defaultValue = "World")String greet) {
		return String.format("Good evening %s, Welcome to batch 254!", greet);
	}


	// Spring Boot S01 Activity
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name",defaultValue = "user")String name) {
		return String.format("hi %s!", name);
	}


	// Cannot(be done for now) be Sir Hahaha
//	@GetMapping("/nameAge")
//	public String nameAge(@RequestParam(value = "Hello",defaultValue = "World")String name, String age) {
//		return String.format("Hello %s! Your age is %s.", name, age);
//	}


}
